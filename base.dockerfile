ARG FROM_IMAGE

FROM $FROM_IMAGE
ARG FROM_IMAGE
ARG APT_PACKAGES
ARG PIP_PACKAGES

LABEL maintainer="CS Technical Officer, NARGA"

USER root
SHELL ["/bin/bash", "-c"]

RUN . /etc/os-release && \
echo -e "\
deb http://ftp.sun.ac.za/ubuntu $VERSION_CODENAME main universe multiverse restricted \n\
deb http://ftp.sun.ac.za/ubuntu $VERSION_CODENAME-security main universe multiverse restricted \n\
deb http://ftp.sun.ac.za/ubuntu $VERSION_CODENAME-updates main universe multiverse restricted \n\
deb http://ftp.sun.ac.za/ubuntu $VERSION_CODENAME-backports main universe multiverse restricted \n\
deb https://ubuntu.mirror.ac.za/ubuntu $VERSION_CODENAME main universe multiverse restricted \n\
deb https://ubuntu.mirror.ac.za/ubuntu $VERSION_CODENAME-security main universe multiverse restricted \n\
deb https://ubuntu.mirror.ac.za/ubuntu $VERSION_CODENAME-updates main universe multiverse restricted \n\
deb https://ubuntu.mirror.ac.za/ubuntu $VERSION_CODENAME-backports main universe multiverse restricted " >> /etc/apt/sources.list 

RUN apt-get update && \
    apt-get install -y htop vim screen tmux curl git-lfs && \
    curl -fsSL https://code-server.dev/install.sh | sh && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/tsl0922/ttyd/releases/latest/download/ttyd.x86_64 -O ttyd && \
    chmod +x ttyd && \
    chown root:root ttyd && \
    mv ttyd /usr/local/bin/

RUN pip --no-cache-dir install --upgrade jupyterlab-git jupyterlab-filesystem-access jupyter-server-proxy

COPY jupyter-codeserver-proxy /tmp/jupyter-codeserver-proxy
RUN pip --no-cache-dir install /tmp/jupyter-codeserver-proxy

COPY jupyter-ttyd-proxy /tmp/jupyter-ttyd-proxy
RUN pip --no-cache-dir install /tmp/jupyter-ttyd-proxy

RUN jupyter labextension install @jupyterlab/server-proxy && \
    jupyter lab build ==minimize=False

RUN \
if [ ! -z "$APT_PACKAGES" ]; then \
    apt-get update && \
    apt-get install -y ${APT_PACKAGES} && \
    rm -rf /var/lib/apt/lists/*; \
fi && \
if [ ! -z "$PIP_PACKAGES" ]; then \
    pip install --no-cache-dir ${PIP_PACKAGES}; \
fi

USER $NB_UID

COPY code-server-settings.json /home/jovyan/.local/share/code-server/Machine/settings.json

RUN echo "${FROM_IMAGE} ${APT_PACKAGES} ${PIP_PACKAGES}"
