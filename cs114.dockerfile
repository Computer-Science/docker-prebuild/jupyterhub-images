ARG FROM_IMAGE

FROM $FROM_IMAGE

LABEL maintainer="CS Technical Officer, NARGA"

USER root

RUN pip --no-cache-dir install --upgrade tk numpy pygame setuptools

RUN curl -L -O https://introcs.cs.princeton.edu/python/code/dist/introcs-1.0.zip && \
    unzip introcs-1.0.zip && rm introcs-1.0.zip && \
    cd introcs-1.0 && \
    python3 setup.py install

USER $NB_UID
