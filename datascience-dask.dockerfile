FROM jupyter/datascience-notebook:2022-09-27

USER root

RUN apt-get update && \
    apt-get install -y htop vim screen tmux graphviz && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install --upgrade \
    jupyterlab-git jupyterlab-filesystem-access \
    dask-labextension jupytext plotly jupyter-dash graphviz

RUN jupyter lab build --minimize=False

USER $NB_UID
