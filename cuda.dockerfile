FROM cschranz/gpu-jupyter:v1.4_cuda-11.6_ubuntu-20.04

USER root

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A4B469963BF863CC && \
    apt-get update && \
    apt-get install -y sssd && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install --upgrade wordcloud pyro-ppl torchvision torchaudio cupy-cuda112 jupyterlab-git jupytext plotly "jupyterlab>=3" "ipywidgets>=7.6" jupyter-dash skimpy pandas-profiling autoviz sweetviz dataprep

RUN jupyter lab build --minimize=False

USER $NB_UID
