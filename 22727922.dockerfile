FROM jupyter/minimal-notebook:2022-09-27

USER root

RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless golang htop vim screen tmux && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install --upgrade jupyterlab-git jupyterlab-filesystem-access

# RUN jupyter lab build --minimize=False

USER $NB_UID

