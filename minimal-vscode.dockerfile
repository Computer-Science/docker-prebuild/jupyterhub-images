FROM jupyter/minimal-notebook:2022-09-27

USER root

RUN apt-get update && \
    apt-get install -y htop vim screen tmux curl && \
    curl -fsSL https://code-server.dev/install.sh | sh && \
    rm -rf /var/lib/apt/lists/*

RUN pip --no-cache-dir install --upgrade jupyterlab-git jupyterlab-filesystem-access jupyter-server-proxy jupyter-vscode-proxy

RUN jupyter lab build --minimize=False

USER $NB_UID
