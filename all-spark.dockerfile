
FROM jupyter/all-spark-notebook:2022-09-27

USER root

RUN pip --no-cache-dir install jupyterlab-git jupytext plotly "jupyterlab>=3" "ipywidgets>=7.6" jupyter-dash skimpy pandas-profiling autoviz sweetviz dataprep

RUN jupyter lab build --minimize=False

USER $NB_UID
