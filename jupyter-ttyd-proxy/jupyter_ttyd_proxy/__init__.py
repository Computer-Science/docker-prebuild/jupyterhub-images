import os
import logging

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

HERE = os.path.dirname(os.path.abspath(__file__))

def get_ttyd_executable(prog):
    from shutil import which

    # check the system path
    if which(prog):
        return prog

    raise FileNotFoundError(f'Could not find {prog} in PATH')

def setup_ttyd():
    """ Setup commands and and return a dictionary compatible
        with jupyter-server-proxy.
    """

    # create command
    cmd = [
        get_ttyd_executable('ttyd'),
        '--max-clients', '10',
        '-t', 'macOptionIsMeta=true',
        '-t', 'logLevel=info',
        '-t', 'disableLeaveAlert=true',
        '-p', '{port}',
        'tmux', 'new', '-A', '-s', 'ttyd', 'bash'
    ]
    logger.info('ttyd command: ' + ' '.join(cmd))

    return {
        'environment': {},
        'command': cmd,
        'absolute_url': False,
        'timeout': 90,
        'new_browser_tab': True,
        'port': 8081,
        'launcher_entry': {
            'enabled': True,
            # 'icon_path': os.path.join(HERE, 'icons/xtermjs.png'),
            'title': 'Web Terminal (ttyd)'
        },
    }
